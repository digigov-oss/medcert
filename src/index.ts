import config from './config.json'; 
import axios from 'axios';
import moment from 'moment';

export type request = {
    "amka": string;
    "application_date": string;
}

export type cert = {
    "speciality": "Δερματολόγος" | "Παθολόγος" | "Ψυχίατρος" | null;
    "insert_date": string | null;
    "result": "ΙΚΑΝΟΣ-Η" | "Δεν βρέθηκε βεβαίωση" | "An error occurred while retrieving the medical certificates."
}

export type certificates = cert[]

//NOTE on authentication error returns text/html!
//error on application_date missing
export type errorResponse = [
    {
        "speciality": null;
        "insert_date": null;
        "result": string; //"An error occurred while retrieving the medical certificates."
    }
]

//error on mailformed application_date
export type errorResponse2 = {
    "timestamp": Date; //"2022-02-11T16:57:26.527+0000",
    "status": number; //400,
    "error": string; //"Bad Request",
    "message": string; //"",
    "path": string; //"/medcert/api/v1/bebaioseis/checkapplication"
}

/**
 * @param {string} amka
 * @param {string} user
 * @param {string} pass
 * @param {string} application_date
 * @param {boolean} prod
 * @returns certificates
 */
export const getCerts = async (amka:string,user:string,pass:string,application_date?:string,prod:boolean=false) => {
    try {
    if (!check_amka(amka)) throw new Error("Invalid amka"); 
    if (application_date && !moment(application_date, "YYYY-MM-DD",true).isValid()) throw new Error("Invalid application_date");
    const url = prod==true? config.prod : config.test;
    const ap:string = application_date ? application_date : moment().format('YYYY-MM-DD');
    const requestBody:request = {
        "amka": amka,
        "application_date": ap
    }
        //axios with basic auth
        const s = await axios.post(url,requestBody,{
            auth: {
                username: user,
                password: pass
            }
        });
        const r:certificates = s.data;
        return {certificates:r,requestBody:requestBody};
       } catch (error:any) {
           return({error:error.message});
       }
}

const check_string = (checkme: string): boolean => {
    if (checkme && typeof checkme === "string") {
      const str2check = checkme.trim()
      if (str2check.length == 0) return false;
      if (str2check === "undefined" || str2check === "null") return false;
      return true
    }
    return false;
  }

const check_amka = (amka: string): boolean => {
    if (!check_string(amka)) return false;
    if (amka=="00000000000") return false;
    // must have 11 chars
    let regexp: RegExp = /^\d{11}$/;
    if (!regexp.test(amka)) return false;
    const luhn_checksum_valid = (value:string) => {
      // The Luhn Algorithm
      let nCheck = 0, bEven = false;
      for (let n = value.length - 1; n >= 0; n--) {
        let cDigit = value.charAt(n),
          nDigit = parseInt(cDigit, 10);  
        if (bEven && (nDigit *= 2) > 9) nDigit -= 9;
        nCheck += nDigit;
        bEven = !bEven;
      }
      return (nCheck % 10) == 0;
    }
    return luhn_checksum_valid(amka);
  }

export default getCerts;