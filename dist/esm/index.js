import config from './config.json';
import axios from 'axios';
import moment from 'moment';
/**
 * @param {string} amka
 * @param {string} user
 * @param {string} pass
 * @param {string} application_date
 * @param {boolean} prod
 * @returns certificates
 */
export const getCerts = async (amka, user, pass, application_date, prod = false) => {
    try {
        if (!check_amka(amka))
            throw new Error("Invalid amka");
        if (application_date && !moment(application_date, "YYYY-MM-DD", true).isValid())
            throw new Error("Invalid application_date");
        const url = prod == true ? config.prod : config.test;
        const ap = application_date ? application_date : moment().format('YYYY-MM-DD');
        const requestBody = {
            "amka": amka,
            "application_date": ap
        };
        //axios with basic auth
        const s = await axios.post(url, requestBody, {
            auth: {
                username: user,
                password: pass
            }
        });
        const r = s.data;
        return { certificates: r, requestBody: requestBody };
    }
    catch (error) {
        return ({ error: error.message });
    }
};
const check_string = (checkme) => {
    if (checkme && typeof checkme === "string") {
        const str2check = checkme.trim();
        if (str2check.length == 0)
            return false;
        if (str2check === "undefined" || str2check === "null")
            return false;
        return true;
    }
    return false;
};
const check_amka = (amka) => {
    if (!check_string(amka))
        return false;
    if (amka == "00000000000")
        return false;
    // must have 11 chars
    let regexp = /^\d{11}$/;
    if (!regexp.test(amka))
        return false;
    const luhn_checksum_valid = (value) => {
        // The Luhn Algorithm
        let nCheck = 0, bEven = false;
        for (let n = value.length - 1; n >= 0; n--) {
            let cDigit = value.charAt(n), nDigit = parseInt(cDigit, 10);
            if (bEven && (nDigit *= 2) > 9)
                nDigit -= 9;
            nCheck += nDigit;
            bEven = !bEven;
        }
        return (nCheck % 10) == 0;
    };
    return luhn_checksum_valid(amka);
};
export default getCerts;
