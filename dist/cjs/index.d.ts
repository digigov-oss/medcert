export declare type request = {
    "amka": string;
    "application_date": string;
};
export declare type cert = {
    "speciality": "Δερματολόγος" | "Παθολόγος" | "Ψυχίατρος" | null;
    "insert_date": string | null;
    "result": "ΙΚΑΝΟΣ-Η" | "Δεν βρέθηκε βεβαίωση" | "An error occurred while retrieving the medical certificates.";
};
export declare type certificates = cert[];
export declare type errorResponse = [
    {
        "speciality": null;
        "insert_date": null;
        "result": string;
    }
];
export declare type errorResponse2 = {
    "timestamp": Date;
    "status": number;
    "error": string;
    "message": string;
    "path": string;
};
/**
 * @param {string} amka
 * @param {string} user
 * @param {string} pass
 * @param {string} application_date
 * @param {boolean} prod
 * @returns certificates
 */
export declare const getCerts: (amka: string, user: string, pass: string, application_date?: string | undefined, prod?: boolean) => Promise<{
    certificates: certificates;
    requestBody: request;
    error?: undefined;
} | {
    error: any;
    certificates?: undefined;
    requestBody?: undefined;
}>;
export default getCerts;
