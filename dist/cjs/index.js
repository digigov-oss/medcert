"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getCerts = void 0;
const config_json_1 = __importDefault(require("./config.json"));
const axios_1 = __importDefault(require("axios"));
const moment_1 = __importDefault(require("moment"));
/**
 * @param {string} amka
 * @param {string} user
 * @param {string} pass
 * @param {string} application_date
 * @param {boolean} prod
 * @returns certificates
 */
const getCerts = (amka, user, pass, application_date, prod = false) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (!check_amka(amka))
            throw new Error("Invalid amka");
        if (application_date && !(0, moment_1.default)(application_date, "YYYY-MM-DD", true).isValid())
            throw new Error("Invalid application_date");
        const url = prod == true ? config_json_1.default.prod : config_json_1.default.test;
        const ap = application_date ? application_date : (0, moment_1.default)().format('YYYY-MM-DD');
        const requestBody = {
            "amka": amka,
            "application_date": ap
        };
        //axios with basic auth
        const s = yield axios_1.default.post(url, requestBody, {
            auth: {
                username: user,
                password: pass
            }
        });
        const r = s.data;
        return { certificates: r, requestBody: requestBody };
    }
    catch (error) {
        return ({ error: error.message });
    }
});
exports.getCerts = getCerts;
const check_string = (checkme) => {
    if (checkme && typeof checkme === "string") {
        const str2check = checkme.trim();
        if (str2check.length == 0)
            return false;
        if (str2check === "undefined" || str2check === "null")
            return false;
        return true;
    }
    return false;
};
const check_amka = (amka) => {
    if (!check_string(amka))
        return false;
    if (amka == "00000000000")
        return false;
    // must have 11 chars
    let regexp = /^\d{11}$/;
    if (!regexp.test(amka))
        return false;
    const luhn_checksum_valid = (value) => {
        // The Luhn Algorithm
        let nCheck = 0, bEven = false;
        for (let n = value.length - 1; n >= 0; n--) {
            let cDigit = value.charAt(n), nDigit = parseInt(cDigit, 10);
            if (bEven && (nDigit *= 2) > 9)
                nDigit -= 9;
            nCheck += nDigit;
            bEven = !bEven;
        }
        return (nCheck % 10) == 0;
    };
    return luhn_checksum_valid(amka);
};
exports.default = exports.getCerts;
