import getCerts from '../src/index';
import config from './config.json'; 

//If you ommit the application_date, the current date will be used.
const test = async () => {
    try {
        const data = await getCerts("15093103933",config.user, config.pass, "2022-02-11", false);
        return data;
    } catch (error:any) {
        console.log(error.message);
    }
}

test().then((certs) => { console.log('certificates',certs); });