# medcert

This service from IDIKA is outside of KED at the moment. It is used to get a medical certification of the person by amka.

#### Usage
```
import getCerts from '@digigov-oss/medcert';
import config from './config.json'; 

//If you ommit the application_date, the current date will be used.
const test = async () => {
    try {
        const data = await getCerts("15093103933",config.user, config.pass);
        return data;
    } catch (error:any) {
        console.log(error.message);
    }
}

test().then((certs) => { console.log('certificates',certs); });
```
#### Returns

an object like the following:
```
{
  certificates: [
    {
      speciality: 'Δερματολόγος',
      insert_date: '2022-02-03 09:15:46.0',
      result: 'ΙΚΑΝΟΣ-Η'
    },
    {
      speciality: 'Παθολόγος',
      insert_date: '2022-02-08 09:15:39.0',
      result: 'ΙΚΑΝΟΣ-Η'
    },
    {
      speciality: 'Ψυχίατρος',
      insert_date: null,
      result: 'Δεν βρέθηκε βεβαίωση'
    }
  ],
  requestBody: { amka: '15093103933', application_date: '2022-02-11' }
}
```
or an error object like the following:
```
{ error: 'Request failed with status code 401' }
```


 